---
layout: handbook-page-toc
title: FY22-Q3 L&D Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


Thanks for reading the 4th edition of the Mental Health newsletter from the GitLab L&D team!

This edition of the newsletter also features a visual highlight newsletter that was shared with team members in Slack.

![newsletter highlights image](/newsletter-highlights.png){: .medium.center}


## GitLab Resource Feature

Have you heard of Modern Health benefits offered to GitLab team members? Check out the [Modern Health handbook page](/handbook/total-rewards/benefits/modern-health/) if you're not aware of the benefit details!

In 2021-09, Modern Health hosted a conference called Elevate 2021. The conference highlighted evolving workforce wellness strategies, the future role of mental health in the workplace, culturally centered care, and applying technology to drive value with new care options.

The conference included some great speakers, including our very own Darren Murph who spoke about `Building Equity Across Types of Workers Within a Hybrid Workforce`.

Other speakers include:

1. Alyson Watson: Founder and CEO, Modern Health
1. Venus Williams: Legendary Tennis Champion, Entrepreneur, and Philanthropist 
1. Ariana Huffington: Founder and CEO, Thrive Global
1. Katrina Lake: Founder and Executive Chairperson, StitchFix
1. Michael Pollan: Best Selling Author & Sustainable Food Advocate

You can watch the recorded conferences on demand on their [Elevate event website](https://www.modernhealth.com/elevate). 

![elevate 2021 image](/elevate-darren.png){: .medium.center}


## Taking time for mental health

2021-10-10 was World Mental Health day. Spring Health shared a one-page guide called [Supporting Mental Health in an Unequal World](https://drive.google.com/file/d/12tytq8qMp4TO4DovKTkrO4ZtNkfvDNbx/view?usp=sharing) that shares data about the inequeties of mental health and health care around the globe, and what you can do to help remove stigmas about mental health.

## Manager resources

You might have seen posts in the #whats-happening-at-gitlab in September from the L&D team about LinkedIn Wellness Week. These posts highlighted a few short LinkedIn Learning videos that might help break up your day and re-focus your attention on your work and your surroundings.

Managers can consider re-sharing theses short videos this week in their team Slack channels!

| Post Topic | Video Links |
| ----- | ----- |
| ![post 1 image](/mental-health-1.png) | [three mindful breaths](https://www.linkedin.com/learning/mindful-meditations-for-work-and-life/three-breaths-practice?u=2255073), 2 minutes, and [preparing yourself for mental agility](https://www.linkedin.com/learning/cultivating-mental-agility/physically-preparing-yourself-for-mental-agility-2?u=2255073), 4 minutes |
| ![post 3 image](/mental-health-3.png) | [Simple shifts for sleep success](https://www.linkedin.com/learning/sleep-is-your-superpower/simple-shifts-for-sleep-success), 4 min and [Relax your brain](https://www.linkedin.com/learning/creativity-tips-for-all-weekly/relax-your-brain), 5 min |
| ![post 4 image](/mental-health-4.png) | [desk hip stretched](https://www.linkedin.com/learning/chair-work-yoga-fitness-and-stretching-at-your-desk/hip-stretches), 5 min and [seated mountain pose](https://www.linkedin.com/learning/chair-work-yoga-fitness-and-stretching-at-your-desk/seated-mountain), 3 min  |

+(post 2 has been omitted as it was a time-sensitive webinar event)


## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health/-/issues/3). 
